//
//  BubblHeaders.h
//  Bubbl
//
//  Copyright © 2017 Bubbl LTD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>

#import <Bubbl/BUMain.h>

//#import <Bubbl/BUVenue.h>
//#import <Bubbl/BUVenue+CoreDataProperties.h>
//
//#import <Bubbl/BUQuestion+CoreDataProperties.h>
//#import <Bubbl/BUCampaign+CoreDataProperties.h>
//#import <Bubbl/BUPayload+CoreDataProperties.h>
//#import <Bubbl/BULocation+CoreDataProperties.h>
//#import <Bubbl/BUDebug+CoreDataProperties.h>
//#import <Bubbl/BUConfig+CoreDataProperties.h>
//#import <Bubbl/BULogLocation+CoreDataProperties.h>

//#import <BubblFramework/UIImageView+bu_ImageCache.h>

//#ifdef TESTING

//#import <Bubbl/NSPersistentStoreCoordinator+BUCoreData.h>
//#import <Bubbl/NSManagedObjectContext+BUCoreData.h>
//#import <Bubbl/NSManagedObject+BUCoreDataCustom.h>
//#import <Bubbl/NSManagedObject+BUCoreData.h>
//#import <Bubbl/BUPayloadLauncher.h>

//#endif

//! Project version number for BubblFramework.
FOUNDATION_EXPORT double BubblFrameworkVersionNumber;

//! Project version string for BubblFramework.
FOUNDATION_EXPORT const unsigned char BubblFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements
// like #import <BubblFramework/PublicHeader.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
