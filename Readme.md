# Bubbl Plugin - Cocoapod


#### Preparing the Release

1. Update the version number in the Podspec file
2. Run a production build of the Bubbl Plugin
3. Copy the build Bubbl.Framework file into this folder
4. Zip the framework

```
zip -r Bubbl.zip LICENSE Bubbl.framework
```
5. Commit the changes and push
6. In the Bitbucket repository click on the Bubbl.zip in the repository and copy the address of the raw zip file.
7. Update the podspec source with the new zip url
8. Commit and push
9. Tag the commit:

```git tag '1.2.3'```

10. Push the tags

```git push --tags```

#### Publish the Pod

Run the following command in the same directory as the .podspec to publish it to the CocoaPods repository:

```
pod trunk push Bubbl.podspec
```